import React from 'react';
import Router from './src/router';
import {Root} from 'native-base'

export default class App extends React.Component {
    state = {
        isLoaded:false
    };
    async componentWillMount() {
        await Expo.Font.loadAsync({
            'Roboto': require('native-base/Fonts/Roboto.ttf'),
            'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
            'Avenir': require('./src/Assets/fonts/avenir/AvenirLTStd-Book.otf'),
            'Myriad Pro': require('./src/Assets/fonts/myriadPro/MyriadPro-Regular.otf'),
        });
        this.setState({isLoaded:true})
    }
  render() {
        let {isLoaded} = this.state;
        if(isLoaded){
          return(<Root><Router/></Root>)
        }else {
            return (null);
        }
  }
};