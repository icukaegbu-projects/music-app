import React from 'react';
import { StyleSheet,ImageBackground} from 'react-native';
import BG from '../Assets/image/background.png'

export default class authContainer extends React.Component {
    render(){
        return (
            <ImageBackground source={BG} style={styles.container}>
                 {this.props.children}
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    },
});
