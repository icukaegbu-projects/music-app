import React from 'react';
import {TouchableOpacity,StatusBar,ScrollView, StyleSheet,Image,View,Text as NativeText } from 'react-native';
import AuthContainer from './AuthContainer';
import Api from '../component/tools/api';
import {Actions} from 'react-native-router-flux';
import { Label, Toast,Item, Input, Icon,Text,Button,Spinner } from 'native-base';
import Logo from '../Assets/image/logo.png'
import Expo from "expo";

export default class login extends React.Component {
    state = {
        firstName:"",
        lastName:"",
        email:"",
        phone:"",
        password:"",
        retypePassword:"",
        isLoading:false
    };
    Submit=async()=>{
        this.setState({isLoading:true});
        Api.login(this.state).then((res)=> {
            if(res.data.success){
                this.setState({isLoading:false});
                Actions.dashWall();
            }
        }).catch((error) =>{
            this.setState({isLoading:false});
            if(error){
                const {success,message} = error.response.data;
                if(success === false){
                    Toast.show({
                        position:'top',
                        type:'danger',
                        text:message,
                        buttonText: ''
                    })
                }else{
                    Toast.show({
                        position:'top',
                        type:'danger',
                        text:"An error occurred, please try again",
                        buttonText: ''
                    })
                }
            }

        });
    };
    handleChange=(name,value)=> {
        this.setState({[name]: value.trim()})
    };
    render(){
        const {isLoading} =this.state;
        let {buttonBg,signUp,footEndContainer,forgotPassword,inputLabel,icon,authButtonText,dontHaveAnAccount,loginContainer,label,logoContainer,authContainer,authInput} = styles;
        return (
            <AuthContainer>
                <StatusBar hidden={true} />
                <View style={authContainer}>
                    <View style={logoContainer} >
                        <Image style={styles.logo} source={Logo} resizeMode="contain"/>
                    </View>
                    <ScrollView style={{flex: 1}} on-drag contentContainerStyle={loginContainer} >
                    <Item floatingLabel style={inputLabel}>
                            <Icon style={icon}  name='ios-person-outline' />
                            <Label style={label}>Email or Phone number</Label>
                            <Input onSubmitEditing={() => this._PasswordInput._root.focus()} onChangeText={(e)=>this.handleChange('email',e)} style={authInput} />
                    </Item>
                        <Item floatingLabel  style={inputLabel}>
                            <Icon style={icon}  name='ios-lock-outline' />
                            <Label style={label}>Password</Label>
                            <Input
                                getRef={(input)=> this._PasswordInput = input}
                                onChangeText={(e)=>this.handleChange('password',e)} secureTextEntry={true} style={authInput}/>
                        </Item>
                        <Button block title="Login" onPress={this.Submit} style={buttonBg}>
                            <Text style={authButtonText}>Login</Text>
                            {isLoading ? <Spinner/> : null}
                        </Button>
                        <TouchableOpacity onPress={()=>{console.log('d')}} >
                            <NativeText style={forgotPassword}>Forgot Password</NativeText>
                        </TouchableOpacity>
                        <View style={footEndContainer}>
                            <View>
                                <Text style={dontHaveAnAccount}>Don't have an account? </Text>
                            </View>
                            <View>
                                <TouchableOpacity onPress={()=>{Actions.register()}} >
                                    <Text style={signUp}>Sign Up</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </AuthContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    footEndContainer:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    label:{
        marginLeft:10,
        color:"#fff"
    },
    dontHaveAnAccount :{
        opacity: 0.5,
        fontFamily: "Avenir",
        fontSize: 17.3,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(255, 255, 255, 0.5)"

    },
    signUp:{
        fontFamily: "Myriad Pro",
        fontSize: 17.3,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#ffffff"
    },
    forgotPassword:{
        opacity: 0.5,
        fontSize:16,
        textAlign:"center",
        fontFamily: "Avenir",
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        marginTop:9,
        marginBottom:8,
        color: "rgba(255, 255, 255, 0.5)"
    },
    authButtonText:{
        fontFamily: "Avenir",
        fontSize: 15,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.86,
        textAlign: "center",
        color: "#ffffff"
    },
    logo:{
        height:102.7,
        width:102.7
    },
    icon:{
        color:"#fff",
        fontSize:40
    },
    alreadyHaveAnAccount:{
        width: 198,
        height: 17.3,
        opacity: 0.5,
        fontFamily: "Avenir",
        fontSize: 17.3,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "rgba(255, 255, 255, 0.5)"
    },
    logoContainer:{
        width: "100%",
        height: "40%",
        flexDirection: 'column',
        alignItems:"center",
        justifyContent:"center"
    },
    authInput :{
        fontFamily: "Avenir",
        fontSize: 15,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.26,
        textAlign: "left",
        color: "#ffffff"
    },
    authContainer:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    buttonBg: {
        backgroundColor: "#0a392e",
        marginBottom:9
    },
    inputLabel:{
        marginBottom:15,
        paddingBottom:10,
        borderColor:"rgba(255,255,255,0.1)",
    },
    loginContainer :{
        width: "100%",
        paddingLeft:30,
        paddingRight:30
    }
});
