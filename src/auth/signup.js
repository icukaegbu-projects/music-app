import React from 'react';
import {TouchableOpacity,StatusBar, StyleSheet,Image,View,ScrollView} from 'react-native';
import AuthContainer from './AuthContainer'
import {Actions} from 'react-native-router-flux';
import {Toast, Label,Body,Left,Right,Item,Header,Spinner, Input, Icon,Text,Button } from 'native-base';
import Logo from '../Assets/image/logo.png'
import Api from '../component/tools/api';
import {Action} from 'react-native-router-flux';

export default class signup extends React.Component {
    state = {
        isLoading:false
    };
    Submit=async()=>{
        this.setState({isLoading:true});
        Api.signUp(this.state).then((res)=> {
            if(res.success){
                this.setState({isLoading:false});
                Actions.dashWall();
            }
        }).catch((error) =>{
            this.setState({isLoading:false});
            console.log(JSON.stringify(error));
            if(error){
                const {success,message} = error.response.data;
                if(success === false){
                    Toast.show({
                        position:'top',
                        type:'danger',
                        text:message,
                        buttonText: ''
                    })
                }else{
                    Toast.show({
                        position:'top',
                        type:'danger',
                        text:"An error occurred, please try again",
                        buttonText: ''
                    })
                }
            }

        });
    };
    handleChange=(name,value)=> {
        this.setState({[name]: value.trim()})
    };
    render(){
        let {isLoading} =this.state;
        let {buttonBg,signUp,logo,footEndContainer,inputLabel,icon,authButtonText,dontHaveAnAccount,loginContainer,label,logoContainer,authContainer,authInput} = styles;
        return (
            <AuthContainer>
                <StatusBar hidden={true} />
                <Header noShadow androidStatusBarColor="#029c66" style={{backgroundColor:"transparent"}}>
                    <Left>
                        <Button title="Go Back"
                                onPress={()=>Actions.pop()} transparent>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                    </Body>
                    <Right/>
                </Header>
                <View style={authContainer}>
                    <View style={logoContainer} >
                        <Image style={logo} source={Logo} resizeMode="contain"/>
                    </View>
                    <ScrollView style={{flex: 1}} on-drag contentContainerStyle={loginContainer} >
                        <Item floatingLabel style={inputLabel}>
                            <Icon style={icon}  name='ios-person-outline' />
                            <Label style={label}>First Name</Label>
                            <Input
                                autoCapitalize="none"
                                onSubmitEditing={() => this.last_name._root.focus()}
                                returnKeyType="next"
                                onChangeText={(e)=>this.handleChange('first_name',e)}
                                style={authInput}
                            />
                        </Item>
                        <Item floatingLabel style={inputLabel}>
                            <Icon style={icon}  name='ios-person-outline' />
                            <Label style={label}>Last Name</Label>
                            <Input  returnKeyType="next"
                                    autoCapitalize="none"
                                    onSubmitEditing={() => this._phoneInput._root.focus()}
                                    getRef={(input)=> this.last_name = input}
                                    onChangeText={(e)=>this.handleChange('last_name',e)}
                                    style={authInput}
                            />
                        </Item>
                        <Item floatingLabel style={inputLabel}>
                            <Icon style={icon}  name='ios-call-outline' />
                            <Label style={label}>Mobile Number</Label>
                            <Input returnKeyType="next"
                                   onSubmitEditing={() => this._MyEmail._root.focus()}
                                   getRef={(input)=> this._phoneInput = input}
                                   keyboardType='numeric'
                                   onChangeText={(e)=>this.handleChange('phone',e)}
                                   style={authInput}
                            />
                        </Item>
                        <Item floatingLabel style={inputLabel}>
                            <Icon style={icon}  name='ios-mail-outline' />
                            <Label style={label}>Email</Label>
                            <Input returnKeyType="next"
                                   onSubmitEditing={() => this._PasswordInput._root.focus()}
                                   getRef={(input)=> this._MyEmail = input}
                                   keyboardType='email-address'
                                   onChangeText={(e)=>this.handleChange('email',e.toLowerCase())}
                                   style={authInput}
                            />
                        </Item>
                        <Item floatingLabel style={inputLabel}>
                            <Icon style={icon}  name='ios-lock-outline' />
                            <Label style={label}>Password</Label>
                            <Input returnKeyType="default"
                                   secureTextEntry
                                   getRef={(input)=> this._PasswordInput = input}
                                   onSubmitEditing={() => this._retypePassword._root.focus()}
                                   onChangeText={(e)=>this.handleChange('password',e)} style={authInput}
                            />
                        </Item>
                        <Item floatingLabel  style={inputLabel}>
                            <Icon style={icon}  name='ios-lock-outline' />
                            <Label style={label}>Confirm password</Label>
                            <Input returnKeyType="default"
                                   secureTextEntry
                                   getRef={(input)=> this._retypePassword = input}
                                   onChangeText={(e)=>this.handleChange('retypePassword',e)}
                                   style={authInput}
                            />
                        </Item>
                        <Button disabled={isLoading} block title="reg" onPress={this.Submit} style={buttonBg}>
                            <Text style={authButtonText}>Join</Text>
                            {isLoading ? <Spinner/> : null}
                        </Button>
                        <View style={footEndContainer}>
                            <View>
                                <Text style={dontHaveAnAccount}>Already have an account? </Text>
                            </View>
                            <View>
                                <TouchableOpacity onPress={()=>{Actions.pop()}} >
                                    <Text style={signUp}>Login</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </AuthContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    footEndContainer:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom:20
    },
    label:{
        marginLeft:10,
        color:"#fff"
    },
    dontHaveAnAccount :{
        opacity: 0.5,
        fontFamily: "Avenir",
        fontSize: 15,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgba(255, 255, 255, 0.5)"

    },
    signUp:{
        fontFamily: "Myriad Pro",
        fontSize: 15,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "#ffffff"
    },
    forgotPassword:{
        opacity: 0.5,
        fontSize:13,
        textAlign:"center",
        fontFamily: "Avenir",
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        marginTop:9,
        marginBottom:8,
        color: "rgba(255, 255, 255, 0.5)"
    },
    authButtonText:{
        fontFamily: "Avenir",
        fontSize: 15,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.86,
        textAlign: "center",
        color: "#ffffff"
    },
    logo:{
        height:102.7,
        width:102.7
    },
    icon:{
        color:"#fff",
        fontSize:30
    },
    alreadyHaveAnAccount:{
        width: 198,
        height: 17.3,
        opacity: 0.5,
        fontFamily: "Avenir",
        fontSize: 14,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "rgba(255, 255, 255, 0.5)"
    },
    logoContainer:{
        width: "100%",
        height: "20%",
        flexDirection: 'column',
        alignItems:"center",
        justifyContent:"center"
    },
    authInput :{
        fontFamily: "Avenir",
        fontSize: 15,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0.26,
        textAlign: "left",
        color: "#ffffff"
    },
    authContainer:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    buttonBg: {
        marginBottom:10,
        backgroundColor: "#0a392e",
        marginTop:9
    },
    inputLabel:{
        paddingBottom:10,
        borderColor:"rgba(255,255,255,0.1)",
    },
    loginContainer :{
        width: "100%",
        paddingLeft:30,
        paddingRight:30
    }
});
