import React,{Component} from 'react';
import Splash from './component/splash/Splash'
import {Router, Stack, Scene,ActionConst} from 'react-native-router-flux';
import {login,dashBoard,signUp} from './component/ImportControl';
import Auth from './component/tools/Auth';
export default class MainRouter extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading:true,
            isLoggedIn:false
        }
    }
  async componentDidMount(){
       let isAuth = await Auth.isUserAuthenticated();
       if(isAuth){
            console.log(isAuth,'from tw');
            this.setState({isLoggedIn:true,isLoading:false})
        }else {
            this.setState({isLoggedIn:false,isLoading:false})
        }

    }
    render() {
        let {isLoading,isLoggedIn} = this.state;
        if(isLoading){
            return (
                <Splash/>
            );
        }else {
            return (
                <Router>
                    <Stack hideNavBar key="root">
                        <Scene key="login" type={ActionConst.RESET} initial={!isLoggedIn} component={login} title="Login"/>
                        <Scene key="register" type={ActionConst.RESET} initial={false} component={signUp} title="Register"/>
                        <Scene key="dashWall" type={ActionConst.RESET}  initial={isLoggedIn} component={dashBoard}/>
                    </Stack>
                </Router>
            );
        }
    }
}
let style = {
    Loadingcontainer: {
        flex: 1,
        // backgroundColor: '#053050',
        justifyContent: 'center',
        alignItems: 'center'
    },
};