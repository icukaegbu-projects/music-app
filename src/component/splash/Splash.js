import React,{Component} from 'react';
import {ImageBackground,Image,StyleSheet,StatusBar} from 'react-native';
import { View,Text } from 'native-base';

export default class Splash extends Component {
  render() {
    const {bgImage,bGcontainer,logoImg,textWrapper,textBreakOne,textBreakTwo,loadingTextWrapper,loadingText} = styles
    return (
      <View>
        <StatusBar hidden={true}/>
        <ImageBackground source={require('../../Assets/image/splash.png')} style={bgImage}>
          <View style={bGcontainer}>
            <Image source={require('../../Assets/image/logo.png')} style={logoImg}/>
          </View>
          <View style={textWrapper}>  
            <Text style={textBreakOne}>CHIKE FOR <Text style={textBreakTwo}>NI<Text style={textBreakOne}>GER</Text><Text style={textBreakTwo}>IA</Text></Text></Text>
          </View> 
          <View style={loadingTextWrapper}>
            <Text style={loadingText}>Please wait...</Text>
          </View>   
        </ImageBackground>
      </View>
   );
  }
}

let styles = StyleSheet.create({
  bgImage: {
    width: "100%",
    height: "100%"
  },

  bGcontainer: {
    position: "absolute", 
    top:"30%", 
    alignItems:"center", 
    width: "100%"
  },

  logoImg: {
    height:154.7,
    width:154.7
  },

  textWrapper: {
    position: "absolute", 
    top:"60%", 
    alignItems:"center", 
    width: "100%"
  },

  textBreakOne: {
    fontSize: 32.7,
    color:"white"
  },

  textBreakTwo: {
    color:"#029c66",
    fontSize: 32.7
  },

  loadingTextWrapper: {
    bottom: "10%",
    position: "absolute", 
    alignItems:"center", 
    width: "100%"
  },
  
  loadingText: {
    color: "white",
    fontStyle: "italic", 
    opacity: 0.5
  }
  
})

