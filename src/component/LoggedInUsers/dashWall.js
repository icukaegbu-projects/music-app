import React,{Component} from 'react';
import {Actions} from 'react-native-router-flux'
import { View,  StyleSheet,Alert } from 'react-native';
import { Header, Left, Body, Right, Button, Icon, Title,Drawer } from 'native-base';
import List from './dashWallSubComponent/list';
import Auth from '../tools/Auth';
import DrawerContent from '../customComponents/DrawerContent';
export default class Home extends Component {
    closeDrawer = () => {
        this.drawer._root.close()
    };
    openDrawer = () => {
        this.drawer._root.open()
    };
    processLogout=()=>{
        Auth.deauthenticateUser().then(()=>{
            Actions.login();
        }).catch((e)=>{
            console.log(e,' from logout button')
        });

    };
    logout=()=>{
        Alert.alert(
            'Logout',
            'Are you sure you want to logout ?',
            [
                {text: 'Cancel', style: 'cancel'},
                {text: 'OK', onPress:this.processLogout},
            ],
            { cancelable: false }
        )
    };
    render() {
        return (
            <Drawer
                panOpenMask={0.2}
                ref={(ref) => { this.drawer = ref; }}
                content={<DrawerContent
                    {...this.state}
                    navigator={this.navigator} />}
                onClose={this.closeDrawer} >
            <View style={{ backgroundColor: "#029c66",flex:1}}>
                <Header androidStatusBarColor="#029c66" style={{backgroundColor:"transparent"}}>
                    <Left>
                        <Button onPress={this.openDrawer} title="goBack" transparent>
                            <Icon name='menu' />
                        </Button>
                    </Left>
                    <Body>
                    <Title>Welcome :Lawrence Nwoko</Title>
                    </Body>
                    <Right>
                        <Button onPress={this.logout} title="logout" transparent>
                            <Icon name='lock' />
                        </Button>
                    </Right>
                </Header>
                <List data={['d','ds','dsf']}/>
            </View>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({

    mainContainer: {
        backgroundColor: "#029c66",
        width: "100%",
        height: "100%"
    },

    header: {
        marginTop: 40,
        flexDirection:"row",
        borderBottomColor: "white",
        borderBottomWidth: 1,
        paddingBottom: 15
    },

    menu: {
        width: "32%",
        marginLeft: 20
    },

    headerTitle: {
        color: "white",
        fontWeight: "bold"
    },

    download: {
        marginTop:30,
        flexDirection: "row"
    },

    downloadInfo: {
        marginRight: 10,
        marginLeft: 20,
        color: "white"
    },

    downloadIconWrapper: {
        alignItems: "flex-end",
        width: "60%",
        paddingRight: 9
    },

    downloadIcon: {
        width: 25,
        height: 25
    },

    diskWrapper: {
        borderRadius: 5,
        backgroundColor: "rgba(0,0,0,0.5)",
        borderColor: "white",
        borderWidth: 1,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 15
    },

    diskInnerWrapper: {
        padding: 10
    },

    diskLastWrapper: {
        borderRadius: 5,
        borderColor: "white",
        borderWidth: 1,
        alignItems: "center",
        padding: 10
    },

    disk: {
        width: 220,
        height: 220
    },

    diskDescription: {
        marginTop: 10,
        marginLeft: 20
    },

    diskLabel: {
        fontWeight: "bold"
    },

    diskDescriptionText: {
        color: "white",
        fontSize: 18
    },

    badge: {
        color: "white",
        padding: 3
    }

});