import React,{Component} from 'react';
import {FlatList} from 'react-native'
import Box from './box';
export default class List extends Component {
    render() {
        let {data} =this.props;
        return (
            <FlatList
                data={data}
                horizontal
                keyExtractor={item => item}
                renderItem={({item}) => <Box info={item}/>}
            />
        );
    }
}
