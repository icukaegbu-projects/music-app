import React,{Component} from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Badge } from 'native-base';

export default class Box extends Component {
    render() {
        return (
            <View style={styles.mainContainer}>
                <View>
                    <View>
                        <View style={styles.download}>
                            <Text style={styles.downloadInfo}>Icon here</Text>
                            <Badge success>
                                <Text style={styles.badge}>2</Text>
                            </Badge>
                            <View style={styles.downloadIconWrapper}>
                                <TouchableOpacity>
                                    <Image style={styles.downloadIcon} source= {require('../../../Assets/image/recordScratch.png')}/>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={styles.diskWrapper}>
                            <View style={styles.diskInnerWrapper}>
                                <View style={styles.diskLastWrapper}>
                                    <TouchableOpacity>
                                        <Image style={styles.disk} source= {require('../../../Assets/image/recordScratch.png')}/>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        <View style={styles.diskDescription}>
                            <Text style={styles.diskDescriptionText}><Text style={styles.diskLabel}>Name:</Text> John Doe</Text>
                            <Text style={styles.diskDescriptionText}><Text style={styles.diskLabel}>Producer: </Text>John Doe</Text>
                            <Text style={styles.diskDescriptionText}><Text style={styles.diskLabel}>Date: </Text>What ever...</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    mainContainer: {
        flex:1,
        marginLeft:30,
        marginRight:30,
        width: "100%",
        height: "100%"
    },

    header: {
        marginTop: 40,
        flexDirection:"row",
        borderBottomColor: "white",
        borderBottomWidth: 1,
        paddingBottom: 15
    },

    menu: {
        width: "32%",
        marginLeft: 20
    },

    headerTitle: {
        color: "white",
        fontWeight: "bold"
    },

    download: {
        marginTop:30,
        flexDirection: "row"
    },

    downloadInfo: {
        marginRight: 10,
        marginLeft: 20,
        color: "white"
    },

    downloadIconWrapper: {
        alignItems: "flex-end",
        width: "60%",
        paddingRight: 9
    },

    downloadIcon: {
        width: 25,
        height: 25
    },

    diskWrapper: {
        borderRadius: 5,
        backgroundColor: "rgba(0,0,0,0.5)",
        borderColor: "white",
        borderWidth: 1,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 15
    },

    diskInnerWrapper: {
        padding: 10
    },

    diskLastWrapper: {
        borderRadius: 5,
        borderColor: "white",
        borderWidth: 1,
        alignItems: "center",
        padding: 10
    },

    disk: {
        width: 220,
        height: 220
    },

    diskDescription: {
        marginTop: 10,
        marginLeft: 20
    },

    diskLabel: {
        fontWeight: "bold"
    },

    diskDescriptionText: {
        color: "white",
        fontSize: 18
    },

    badge: {
        color: "white",
        padding: 3
    }

});