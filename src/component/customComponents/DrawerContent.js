import React from "react";
import {  ImageBackground,Image,View } from "react-native";
import {Actions} from 'react-native-router-flux';
import { Container, Content, Text, List, ListItem,Icon } from "native-base";
const routes = [{"data":"Upload",icon:"ios-cloud-upload-outline",type:"Ionicons" },{"data":"User Profile",icon:"ios-cloud-download-outline",type:"Ionicons" } ,{"data":"Settings",icon:"ios-settings-outline",type:"Ionicons" }];
export default class SideBar extends React.Component {
    render() {
        let {accttype,balance} = this.props;
        return (
            <Container >
                <Content style={{backgroundColor:"#ebebeb"}}>
                    <ImageBackground
                        source={require('../../Assets/image/background.png')}
                        style={{
                            height: 130,
                            alignSelf: "stretch",
                            justifyContent: "center",
                            alignItems: "center"
                        }}>
                        <Image
                            style={{
                                height: 130,
                                alignSelf: "stretch",
                                justifyContent: "center",
                                alignItems: "center"
                            }}
                            resizeMode="contain"
                            source={require('../../Assets/image/logo.png')}
                        />
                        {/*<Text style={{color:"#fff",textAlign:"center",bottom:1,position:"absolute" }}>  Account Type:{accttype}</Text>*/}
                        {/*<View style={style.circle} >*/}
                            {/*<Text style={{color:"#fff",textAlign:"center" }}>₦{balance}</Text>*/}
                        {/*</View>*/}
                    </ImageBackground>
                    <List
                        dataArray={routes}
                        renderRow={data => {
                            return (
                                <ListItem
                                    button
                                    onPress={() =>Actions[data.data]}>
                                    <Icon type={data.type} style={style.Sideicon}  name={data.icon} />
                                    <Text>{" "+data.data}</Text>
                                </ListItem>
                            );
                        }}
                    />
                </Content>
            </Container>
        );
    }
}
let style = {
    Sideicon:{
      color:"#707070"
    },
    circle: {
        width: 100,
        height: 100,
        borderRadius: 100/2,
        position:"absolute",
        top:0,
        borderWidth: 3,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width:4, height: 4 },
        shadowOpacity: 0.6,
        shadowRadius: 6,
        elevation: 3,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        flex:1,
        justifyContent: "center",
        alignItems: "center"
    }
};
