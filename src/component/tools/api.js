import axios from "axios"
import Auth from '../tools/Auth'
if(__DEV__){
    axios.defaults.baseURL = 'http://192.168.43.242:4000/api';
}else {
    axios.defaults.baseURL = 'http://exampe.com.ng/';
}
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
class Api {
    static async login (data) {
        try{
            let result = await axios.post('/user/login',data);
            if(result.data.success){
                console.log(result.data)
                await Auth.authenticateUser(result.data.token,result.data.fullname);
                console.log("Lawence")
                return result
            }
        }
       catch (err) {
           throw {...err}
       }
    }

    static async signUp (data) {
        try {
            let result = await axios.post('/user/register',data);
            if(result.data.success){
                let loginData = await this.login(data);
                if(loginData.data.success){
                    return {success:true}
                }
            }
        }
        catch (err) {
            throw {...err}
        }
    }
}
export default Api