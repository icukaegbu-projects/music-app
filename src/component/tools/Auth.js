import Expo from 'expo'
console.log(Expo.SecureStore.getItemAsync('token'))
class Auth {
     static async authenticateUser (token,fullname) {
        await Expo.SecureStore.setItemAsync("token", token);
        await Expo.SecureStore.setItemAsync("fullname", fullname);
    }
    static async isUserAuthenticated() {
        return await Expo.SecureStore.getItemAsync('token') !== null
    }
    static async deauthenticateUser() {
         await Expo.SecureStore.deleteItemAsync('token');
         await Expo.SecureStore.deleteItemAsync("fullname");
    }
    static async getToken() {
        return await Expo.SecureStore.getItemAsync('token');
    }
}
export default Auth;
