import login from '../auth/login'
import signUp from '../auth/signup'
import dashBoard from './LoggedInUsers/dashWall'
export {
    login,
    signUp,
    dashBoard
}