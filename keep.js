import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Header,Right, View, DeckSwiper, Card, CardItem, Thumbnail, Text, Left, Body, Icon,Button } from 'native-base';
import {Actions} from "react-native-router-flux";
const cards = [
    {
        text: 'Card One',
        name: 'One',
        image: require('../../Assets/image/recordScratch.png'),
    },
];
export default class dashBoard extends Component {
    render() {
        return (
            <View style={{flex:1}}>
                <Header androidStatusBarColor="#029c66" style={{backgroundColor:"#029c66"}}>
                    <Left>
                        <Button title="Go Back"
                                onPress={()=>Actions.pop()} transparent>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                    </Body>
                    <Right/>
                </Header>
                {/*Header End*/}
                <View style={{flex:1}}>
                    <DeckSwiper
                        ref={(c) => this._deckSwiper = c}
                        dataSource={cards}
                        renderEmpty={() =>
                            <View style={{ alignSelf: "center" }}>
                                <Text>Over</Text>
                            </View>}
                        renderItem={item =>
                            <Card style={{ elevation: 3 }}>
                                <CardItem>
                                    <Left>

                                        <Thumbnail source={item.image} />
                                        <Body>
                                        <Text>{item.text}</Text>
                                        <Text note>NativeBase</Text>
                                        </Body>
                                    </Left>
                                </CardItem>
                                <CardItem cardBody>
                                    <Image style={{ height: 300, flex: 1 }} source={item.image} />
                                </CardItem>
                                <CardItem>
                                    <Icon name="heart" style={{ color: '#ED4A6A' }} />
                                    <Text>{item.name}</Text>
                                </CardItem>
                            </Card>
                        }/>
                </View>
            </View>);
    }
}